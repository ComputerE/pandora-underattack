package com.pandora.logic;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JSONWriter{
	public static void main(String[] args)throws IOException {
		JSONObject obj= new JSONObject();
		obj.put("Name", "crunchify.com");
		obj.put("Author", "App Shaih");
  
		JSONArray company = new JSONArray();
		company.add("Company: eBay");
		company.add("Company: Paypal");
		company.add("Company: Google");
		obj.put("Company List", company);
  
		FileWriter file = new FileWriter("D:\\document.json");
		try{
			file.write(obj.toJSONString());
			System.out.println("Successfully Copied JSON object to file...");
			System.out.println("\nJSON Object: "+ obj);
   
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			file.flush();
			file.close();
		}
	}
}