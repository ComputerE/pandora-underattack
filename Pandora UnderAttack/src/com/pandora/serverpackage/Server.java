package com.pandora.serverpackage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class Server{
	
	protected PrintWriter output;
	protected BufferedReader input;
	protected Object entrada;
	
	public static void main(String[] args){
		new Server();
	}
	public Server(){
		
		try {
			ServerSocket sSocket = new ServerSocket(8080);
			System.out.println("Server started at: "+ new Date());
			
			while(true){
				Socket socket = sSocket.accept();
				ClientThread client = new ClientThread(socket);
				new Thread(client).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public class ClientThread implements Runnable{
		
		Socket threadSocket;
		
		public ClientThread(Socket socket){
			threadSocket = socket;
			
		}
		
		@Override
		public void run() {
			try {
				output = new PrintWriter(threadSocket.getOutputStream(),true);
				input = new BufferedReader(new InputStreamReader(threadSocket.getInputStream()));
				ServerReader reader = new ServerReader();
				reader.startThread();
				ServerWriter writer = new ServerWriter();
				writer.startThread();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		
	public class ServerReader implements Runnable{
		
		public void startThread(){
			Thread thread = new Thread();
			thread.start();
		}

		@Override
		public void run() {
			while(true){
				try{
					// entrada is type Object... it is a test
					entrada = input.readLine();
					if(entrada!=null){
						//TODO poner aqui lo q debe hacer con la entrada
					}
					
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			
		}
		
	}
	public class ServerWriter implements Runnable{
		
		public void startThread(){
			Thread thread = new Thread();
			thread.start();
		}
		@Override
		public void run() {
			try{
				//TODO Aqui va todo lo que debe mandar
				
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
}

}
